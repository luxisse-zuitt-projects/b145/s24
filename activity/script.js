let users = `[
	{
		"firstname":"Monika",
		"lastname":"Shin",
		"email":"monikashin@email.com",
		"password":"womanifesto",
		"isAdmin":true,
		"mobileNumber":"01234567891"
	},

	{
		"firstname":"Hyowon",
		"lastname":"Jo",
		"email":"lipj@email.com",
		"password":"wackingqueen",
		"isAdmin":false,
		"mobileNumber":"02345678910"
	}

]`;

let orders = `[
	{
		"userId":1,
		"transactionDate":"01-16-22",
		"status":"paid",
		"total":1
	},

	{
		"userId":2,
		"transactionDate":"01-16-22",
		"status":"shipped",
		"total":1
	}
]`;

let products = `[
	{
		"name":"Herschel Backpack",
		"description":"Your perfect pack for everyday use and walks in the forest.",
		"price":80,
		"stocks":120,
		"isActive":true,
		"sku":"PWOMN-22-16-01"
	},

	{
		"name":"WD 4TB Portable External Hard Drive",
		"description":"Expand your PS4 gaming experience. Fast and easy setup with high capacity.",
		"price":115,
		"stocks":400,
		"isActive":true,
		"sku":"PS4-22-17-01"
	}
]`;

let orderProducts = `[
	{
		"orderId":1,
		"productId":1,
		"quantity":2,
		"price":80,
		"subTotal":160
	}
]`;

console.log(JSON.parse(products));